package blundr;

/**
 * This is the implementation of a class blunder It represents the users in a
 * blunder
 * 
 * @author nchitukula
 *
 */
public class User {
    private String name;        //represents the name of the user
    private int    age;         // represents the age of the user
    private String profession;  // represents the profession of the user
    private String hobbies;     // represents the hobbies of the user
    
  /** 
   * Parameterized Constructor  
   * @param name
   * @param age
   * @param profession
   * @param hobbies
   */
    public User(String name, int age, String profession, String hobbies) {
    	
    	this.name      = name;
    	this.age       = age;
    	this.profession= profession;
    	this.hobbies   = hobbies;
    	System.out.println(this);
    }
/** 
 * Overriding the toString of user Object   
 */
 @Override
   public String toString() {
	 String stringtoreturn = "The above are the User details :\n";
	 stringtoreturn+="username       :  "+this.name+"\n";
	 stringtoreturn+="userage        :  "+this.age+"\n";
	 stringtoreturn+="userprofession :  "+this.profession+"\n";
	 stringtoreturn+="userhobbies    :  "+this.hobbies+"\n";
	 return stringtoreturn;
    } 
 }
   
    
