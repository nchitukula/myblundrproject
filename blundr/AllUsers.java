package blundr;

import java.util.ArrayList;

//import java.util.ArrayList;

public class AllUsers {
	// dynamic list to contain all users in the system
	private ArrayList<User>userList;
	     // default constructor
	public AllUsers() {
		this.userList = new ArrayList<User>();
	}
	
/** This is a helper method to create user
 * 	
 * @param name
 * @param age
 * @param profession
 * @param hobbies
 * @param string3 
 * @param string2 
 * @param string 
 * @return
 */


    public boolean createNewBlundrUser(String name, int age, String profession, String hobbies) {
	   
  		   User addNewBlundrUser  =  new User(name, age, profession, hobbies);
  		   this.userList.add(addNewBlundrUser);
  		      return true;
		   
    	
	    }  
  }	
	

	  
  